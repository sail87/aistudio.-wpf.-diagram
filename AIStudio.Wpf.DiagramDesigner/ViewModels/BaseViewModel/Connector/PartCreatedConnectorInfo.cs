﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class PartCreatedConnectorInfo : ConnectorInfoBase
    {
        public PartCreatedConnectorInfo(double X, double Y) : this(null, X, Y)
        {

        }

        public PartCreatedConnectorInfo(IDiagramViewModel root, double X, double Y) : base(root, ConnectorOrientation.None)
        {
            this._position = new PointBase(X, Y);
        }



        private PointBase _position;
        public override PointBase Position
        {
            get
            {
                return _position;
            }
            set
            {
               
                SetProperty(ref _position, value);
            }
        }
    }
}
