﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum CellHorizontalAlignment
    {
        None,
        Left,
        Center,
        Right
    }

    public enum CellVerticalAlignment
    {
        None,
        Top,
        Center,
        Bottom
    }
}
