﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConectorStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                if (parameter is Style style)
                {
                    return style;
                }
                else if (parameter is string str)
                {
                    return Application.Current.FindResource(str) as Style;
                }
                else
                {
                    return Application.Current.FindResource("DefaultConnectorStyle") as Style;
                }
            }

            return value;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
