﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Helpers;

namespace AIStudio.Wpf.Flowchart.Models
{
    public class FlowchartToolBoxData : ToolBoxData
    {
        public NodeKinds Kind
        {
            get; set;
        }

        public FlowchartToolBoxData(NodeKinds kind, Type type, double width, double height, Size? desiredSize) : base(kind.GetDescription(), null, type, width, height, desiredSize)
        {
            Kind = kind;
            ColorViewModel.LineColor.Color = Colors.Black;
        }

    }
}
