﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.Script.Core.Models;
using AIStudio.Wpf.Script.Core.ViewModels;
using System.Collections.Generic;

namespace AIStudio.Wpf.Script.ViewModels
{
    public class ColorBoxCollectorScriptViewModel : RoslynScriptViewModel
    {
        public ColorBoxCollectorScriptViewModel()
        {
        }

        public ColorBoxCollectorScriptViewModel(IDiagramViewModel root) : base(root)
        {
        }

        public ColorBoxCollectorScriptViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public ColorBoxCollectorScriptViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {
        }

        protected override void InitNew()
        {
            base.InitNew();

            ItemWidth = 230;
            ItemHeight = 76;
            Code = @"using System;
using System.Collections.Generic;
using AIStudio.Wpf.Script.Core.Models;
using System.Windows.Media;
using System.Linq;

namespace AIStudio.Wpf.CSharpScript
{
    public class ColorBoxFactory
    {   
        private List<ColorBoxModel> ItemsSource { get; set;} = new List<ColorBoxModel>();
        public ColorBoxModel Input {private get; set;}
        public int Count{ get; private set;}

        public void Execute()
        {
        	if (Input != null)
            {
                if(ItemsSource.Count >= 100)
                {
                    ItemsSource.Clear();
                }
                ItemsSource.Add(Input);
                Input = null;
                Console.WriteLine($""收集到{Input.Text}号Box"");
            }     
            
            Count = ItemsSource.Count;
        }
    } 
}";
        }

        private List<ColorBoxModel> _itemsSource;
        public List<ColorBoxModel> ItemsSource
        {
            get
            {
                return _itemsSource;
            }
            set
            {
                SetProperty(ref _itemsSource, value);
            }
        }

        public ColorBoxModel Tag { get; set; }

    }
}
