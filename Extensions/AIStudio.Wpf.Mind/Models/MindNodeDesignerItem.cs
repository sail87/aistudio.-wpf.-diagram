﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml.Serialization;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Mind.ViewModels;
using Newtonsoft.Json;

namespace AIStudio.Wpf.Mind.Models
{
    [Serializable]
    [XmlInclude(typeof(MindNodeDesignerItem))]
    public class MindNodeDesignerItem : DesignerItemBase
    {
        public MindNodeDesignerItem()
        {

        }
        public MindNodeDesignerItem(MindNode item) : base(item)
        {
            MindType = item.MindType;
            MindTheme = item.MindTheme;
            Spacing = item.Spacing;
            Offset = item.Offset;
            IsExpanded = item.IsExpanded;
            LinkInfoItem = new LinkInfoItem(item.LinkInfo);
            ImageInfoItem = new ImageInfoItem(item.ImageInfo);
            Remark = item.Remark;
            Priority= item.Priority?.ToString();
            Rate=item.Rate.ToString();
            if (item.Tags != null)
            {
                Tags = new List<string>(item.Tags);
            }
        }

        [XmlAttribute]
        public MindType MindType
        {
            get; set;
        }

        [XmlAttribute]
        public MindTheme MindTheme
        {
            get; set;
        }

        [XmlIgnore]
        public Size Spacing
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("Spacing")]
        public string XmlSpacing
        {
            get
            {
                return SerializeHelper.SerializeSize(Spacing);
            }
            set
            {
                Spacing = SerializeHelper.DeserializeSize(value);
            }
        }


        [XmlIgnore]
        public Point Offset
        {
            get; set;
        }

        [JsonIgnore]
        [XmlAttribute("Offset")]
        public string XmlOffset
        {
            get
            {
                return SerializeHelper.SerializePoint(Offset);
            }
            set
            {
                Offset = SerializeHelper.DeserializePoint(value);
            }
        }

        [XmlAttribute]
        public bool IsExpanded
        {
            get; set;
        }

        [XmlElement]
        public LinkInfoItem LinkInfoItem
        {
            get;set;
        }

        [XmlElement]
        public ImageInfoItem ImageInfoItem
        {
            get; set;
        }
        [XmlAttribute]
        public string Remark
        {
            get; set;
        }
        [XmlAttribute]
        public string Priority
        {
            get; set;
        }
        [XmlAttribute]
        public string Rate
        {
            get; set;
        }

        [XmlArray]
        public List<string> Tags
        {        
            get; set;
        } 
    }

    [Serializable]
    [XmlInclude(typeof(LinkInfoItem))]
    public class LinkInfoItem
    {
        public LinkInfoItem()
        {

        }

        public LinkInfoItem(LinkInfo linkinfo) 
        {
            Url = linkinfo?.Url;
            Text = linkinfo?.Text;
        }

        [XmlAttribute]
        public string Url
        {
            get;set;
        }

        [XmlAttribute]
        public string Text
        {
            get; set;
        }
    }

    [Serializable]
    [XmlInclude(typeof(ImageInfoItem))]
    public class ImageInfoItem
    {
        public ImageInfoItem()
        {

        }

        public ImageInfoItem(ImageInfo imageInfo)
        {
            Url = imageInfo?.Url;
            Text = imageInfo?.Text;
        }

        public string Url
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }
    }
}
