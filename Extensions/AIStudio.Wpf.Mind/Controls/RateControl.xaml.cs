﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIStudio.Wpf.Mind.Controls
{
    /// <summary>
    /// RateControl.xaml 的交互逻辑
    /// </summary>
    public class RateControl : Control
    {
        static RateControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RateControl), new FrameworkPropertyMetadata(typeof(RateControl)));
        }

        /// <summary>Identifies the <see cref="Rate"/> dependency property.</summary>
        public static readonly DependencyProperty RateProperty
            = DependencyProperty.Register(nameof(Rate),  typeof(double?), typeof(RateControl));

        /// <summary> 
        /// Whether or not the "popup" menu for this control is currently open
        /// </summary>
        public double? Rate
        {
            get => (double?)this.GetValue(RateProperty);
            set => this.SetValue(RateProperty, (double?)value);
        }
    }
}
