﻿using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramApp.ViewModels;
using AIStudio.Wpf.Flowchart.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Mind.ViewModels;
using AIStudio.Wpf.Mind;
using AIStudio.Wpf.DiagramDesigner.Additionals;
using AIStudio.Wpf.Mind.Helpers;
using AIStudio.Wpf.Mind.Models;

namespace AIStudio.Wpf.DiagramApp.ViewModels
{
    public class MindViewModel : PageViewModel
    {
        public MindViewModel(string title, string status, DiagramType diagramType, MindType mindType) : base(title, status, diagramType, mindType.ToString())
        {

        }
        public MindViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {

        }

        protected override void OpenFile(DiagramDocument diagramDocument, string ext)
        {
            base.OpenFile(diagramDocument, ext);
            foreach (var vm in DiagramViewModels)
            {
                vm.Init(false);
            }
            if (MindDiagramViewModel != null)
            {
                SubType = MindDiagramViewModel.MindType.ToString();
                MindTheme = MindDiagramViewModel.MindTheme;
            }
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.DiagramOption.LayoutOption.GridCellSize = new Size(100, 100);
            DiagramViewModel.DiagramOption.LayoutOption.ShowGrid = false;
            DiagramViewModel.DiagramOption.LayoutOption.AllowDrop = false;
        }

        public IMindDiagramViewModel MindDiagramViewModel
        {
            get
            {
                return DiagramViewModel as IMindDiagramViewModel;
            }
        }

        public MindType MindType
        {
            get
            {
                if (SubType == null)
                {
                    return MindType.Mind;
                }
                else
                {
                    return SubType.ToEnum<MindType>();
                }
            }
        }

        public MindTheme MindTheme
        {
            get;set;
        }

        protected override void Init(bool initNew)
        {
            DiagramViewModels = new ObservableCollection<IDiagramViewModel>()
            {
                GetDiagramViewModel("页-1", DiagramType, initNew),
            };
            DiagramViewModel = DiagramViewModels.FirstOrDefault();

            InitDiagramViewModel();
            var level1node = MindDiagramViewModel.RootItems.FirstOrDefault();

            MindNode level2node1_1 = new MindNode(DiagramViewModel) { Text = "分支主题1" };
            level2node1_1.AddTo(level1node, 0, false);

            MindNode level2node1_1_1 = new MindNode(DiagramViewModel) { Text = "分支主题1_1" };
            level2node1_1_1.AddTo(level2node1_1, 0, false);

            MindNode level2node1_1_2 = new MindNode(DiagramViewModel) { Text = "分支主题1_2" };
            level2node1_1_2.AddTo(level2node1_1, 0, false);

            MindNode level2node1_1_3 = new MindNode(DiagramViewModel) { Text = "分支主题1_3" };
            level2node1_1_3.AddTo(level2node1_1, 0, false);

            MindNode level2node1_2 = new MindNode(DiagramViewModel) { Text = "分支主题2" };
            level2node1_2.AddTo(level1node, 0, false);

            MindNode level2node1_3 = new MindNode(DiagramViewModel) { Text = "分支主题3" };
            level2node1_3.AddTo(level1node, 0, false);

            DiagramViewModel.Init(initNew);
        }

        protected override DiagramViewModel GetDiagramViewModel(string name, DiagramType diagramType, bool initNew)
        {
            var viewmodel = new MindDiagramViewModel() { Name = name ?? NewNameHelper.GetNewName(DiagramViewModels.Select(p => p.Name), "页-"), DiagramType = diagramType, MindType = MindType, MindTheme = MindTheme };
            viewmodel.Init(initNew);

            return viewmodel;
        }

        public override void Dispose()
        {
            base.Dispose();


        }
    }
}
