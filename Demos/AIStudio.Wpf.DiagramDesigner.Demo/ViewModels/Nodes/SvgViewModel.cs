﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class SvgViewModel : BaseViewModel
    {
        public SvgViewModel()
        {
            Title = "SVG Nodes";
            Info = "You can also have SVG nodes! All you need to do is to set the Layer to RenderLayer.SVG.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.DiagramOption.LayoutOption.CellVerticalAlignment = CellVerticalAlignment.Center;
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            DiagramViewModel.Add(node1);

            PathItemViewModel node2 = new PathItemViewModel(DiagramViewModel) { Left = 300, Top = 300, Icon = "M 0 -50 L 50 -50 L 50 -10 A 1 1 0 0 0 50 10 L 50 50 L 0 50 L 0 10 A 1 1 0 0 0 0 -10 Z" };
            DiagramViewModel.Add(node2);

            PathItemViewModel node3 = new PathItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Icon = "M 0 -50 L 50 -50 L 50 -10 A 1 1 0 0 0 50 10 L 50 50 L 0 50 L 0 10 A 1 1 0 0 0 0 -10 Z" };
            DiagramViewModel.Add(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.TopConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.BottomConnector, node3.BottomConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            DiagramViewModel.Add(connector2);
        }
    }
}
