﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class ColoredPortViewModel : BaseViewModel
    {
        public ColoredPortViewModel()
        {
            Title = "Colored port";
            Info = "Creating your own custom ports is very easy!" +
                "In this example, you can only attach links from/to ports with the same color.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            node1.ClearConnectors();
            node1.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node1, ConnectorOrientation.Top, Colors.Red));
            node1.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node1, ConnectorOrientation.Bottom, Colors.Red));
            node1.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node1, ConnectorOrientation.Left, Colors.Green));
            node1.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node1, ConnectorOrientation.Right, Colors.Green));
            DiagramViewModel.Add(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 300, Text = "2" };
            node2.ClearConnectors();
            node2.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node2, ConnectorOrientation.Top, Colors.Red));
            node2.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node2, ConnectorOrientation.Bottom, Colors.Red));
            node2.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node2, ConnectorOrientation.Left, Colors.Green));
            node2.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node2, ConnectorOrientation.Right, Colors.Green)); 
            DiagramViewModel.Add(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Text = "3" };
            node3.ClearConnectors();
            node3.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node3, ConnectorOrientation.Top, Colors.Red));
            node3.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node3, ConnectorOrientation.Bottom, Colors.Red));
            node3.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node3, ConnectorOrientation.Left, Colors.Green));
            node3.AddConnector(new ColoredPortConnectorInfo(DiagramViewModel, node3, ConnectorOrientation.Right, Colors.Green));
            DiagramViewModel.Add(node3);
        }
    }

    public class ColoredPortConnectorInfo : FullyCreatedConnectorInfo
    {
        public ColoredPortConnectorInfo(DesignerItemViewModelBase dataItem, ConnectorOrientation orientation, Color color, bool isInnerPoint = false, bool isPortless = false) : base(dataItem, orientation, isInnerPoint, isPortless)
        {
            ColorViewModel.FillColor.Color = color;
        }

        public ColoredPortConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, ConnectorOrientation orientation, Color color, bool isInnerPoint = false, bool isPortless = false) : base(root, dataItem, orientation, isInnerPoint, isPortless)
        {
            ColorViewModel.FillColor.Color = color;
        }

        public ColoredPortConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, SelectableItemBase designer) : base(root, dataItem, designer)
        {
        }

        public ColoredPortConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, SerializableItem serializableItem, string serializableType) : base(root, dataItem, serializableItem, serializableType)
        {
        }

        public override bool CanAttachTo(ConnectorInfoBase port)
        {
            // Checks for same-node/port attachements
            if (!base.CanAttachTo(port))
                return false;

            // Only able to attach to the same port type
            if (!(port is ColoredPortConnectorInfo cp))
                return false;

            return ColorViewModel.FillColor.Color == cp.ColorViewModel.FillColor.Color;// 颜色一样才能连接
        }

    }
}
