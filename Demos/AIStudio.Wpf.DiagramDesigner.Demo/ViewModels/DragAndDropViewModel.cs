﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using AIStudio.Wpf.Flowchart;
using AIStudio.Wpf.Flowchart.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class DragAndDropViewModel: BaseViewModel
    {
        public DragAndDropViewModel()
        {
            Title = "Drag & Drop";
            Info = "A very simple drag & drop of flowchart demo.";

            ToolBoxViewModel = new FlowchartToolBoxViewModel();
            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.ShowGrid = true;
            DiagramViewModel.DiagramOption.LayoutOption.GridCellSize = new Size(100, 100);
            DiagramViewModel.DiagramOption.LayoutOption.GridMarginSize = new Size(0, 0);
            DiagramViewModel.DiagramOption.LayoutOption.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.DiagramOption.LayoutOption.CellVerticalAlignment = CellVerticalAlignment.Center;
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);

            DiagramViewModel.PropertyChanged += DiagramViewModel_PropertyChanged;
        }

        public FlowchartToolBoxViewModel ToolBoxViewModel
        {
            get; private set;
        }

        public SelectableDesignerItemViewModelBase SelectedItem
        {
            get
            {
                return DiagramViewModel.SelectedItems?.FirstOrDefault();
            }
        }

        private List<SelectOption> _users = new List<SelectOption>()
        {
            new SelectOption(){ value = "操作员1",text = "操作员1" },
            new SelectOption(){ value = "操作员2",text = "操作员2" },
            new SelectOption(){ value = "Admin",text = "Admin" },
        };
        public List<SelectOption> Users
        {
            get
            {
                return _users;
            }
            set
            {
                _users = value;
            }
        }

        private List<SelectOption> _roles = new List<SelectOption>()
        {
            new SelectOption(){ value = "操作员",text = "操作员" },
            new SelectOption(){ value = "管理员",text = "管理员" },
        };
        public List<SelectOption> Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
            }
        }     

        private void DiagramViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                RaisePropertyChanged(nameof(SelectedItem));
            }
        }
    }
}
